package instagram;

import instagram.constant.Settings;
import instagram.thread.AbstractCommentator;
import instagram.thread.CommentatorBatch;
import instagram.thread.CommentatorDogger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static instagram.constant.Settings.MAX_PAUSE_MILLS;
import static instagram.constant.Settings.MIN_PAUSE_MILLS;
import static instagram.constant.Settings.PATH_TO_CHROME_DRIVER;

public class Main
{
    public static void main(String[] args) throws InterruptedException, IOException
    {
        new ProcessBuilder(PATH_TO_CHROME_DRIVER).start();
        PropertyConfigurator.configure(new File(Settings.LOG4J_PROPERTIES_PATH).getAbsolutePath());
        ExecutorService executor = Executors.newFixedThreadPool(6);
        AbstractCommentator.setStartTime(Instant.now());
        Properties appProps = new Properties();
        appProps.load(Main.class.getResourceAsStream("/credentials.properties"));
        System.out.println(executor.submit(new CommentatorDogger(MIN_PAUSE_MILLS / 2, MAX_PAUSE_MILLS / 2,
                appProps.getProperty("login_alexey"),
                appProps.getProperty("password_alexey"))));
        TimeUnit.MINUTES.sleep(5);
        System.out.println(executor.submit(new CommentatorDogger(MIN_PAUSE_MILLS / 2, MAX_PAUSE_MILLS / 2,
                appProps.getProperty("login_newway"),
                appProps.getProperty("password_newway"))));
        TimeUnit.MINUTES.sleep(5);
        System.out.println(executor.submit(new CommentatorDogger(MIN_PAUSE_MILLS / 2, MAX_PAUSE_MILLS / 2,
                appProps.getProperty("login_una"),
                appProps.getProperty("password_una"))));
    }
}