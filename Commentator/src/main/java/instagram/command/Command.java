package instagram.command;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public interface Command {
    void execute(WebDriver driver, Actions actions) throws InterruptedException;
}
