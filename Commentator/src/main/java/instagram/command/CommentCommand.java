package instagram.command;

import instagram.constant.CSSselectors;
import instagram.scanner.CommentProcessor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class CommentCommand implements Command
{

    private String comment;

    public CommentCommand(String comment) {
        this.comment = comment;
    }

    @Override
    public void execute(WebDriver driver, Actions actions) throws InterruptedException {
        WebElement commentsField = new CommentProcessor(driver).locateElement(CSSselectors.COMMENTS_FIELD_2);
        actions.moveToElement(commentsField).click();
        actions.sendKeys(comment);
        WebElement commentsForm = driver.findElement(CSSselectors.COMMENT_SUBMIT);
        actions.moveToElement(commentsForm).click();
        actions.perform();
        commentsForm.submit();
    }
}
