package instagram.command;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.LinkedHashSet;
import java.util.Set;

@Slf4j
public class Macro {
    private final Set<Command> commands;
    private WebDriver driver;
    private Actions actions;

    public Macro(WebDriver driver, Actions actions) {
        commands = new LinkedHashSet<>();
        this.driver = driver;
        this.actions = actions;
    }

    public void record(Command command) {
        commands.add(command);
    }

    public void run() {
        commands.forEach(c -> {
            try {
                c.execute(driver, actions);
            } catch (InterruptedException e) {
                log.error("Unexpected error", e);
            }
        });
    }
}
