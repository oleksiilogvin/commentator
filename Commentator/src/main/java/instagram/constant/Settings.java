package instagram.constant;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class Settings
{

    public static final Map<String, String> promos = new HashMap<>();
    public static Set<String> dogs = new HashSet<>();

    static
    {
        promos.put("https://www.instagram.com/p/Bn05Bu5BwpN/?taken-by=mastercardukraine", "Mastercardukraine");
    }

    public static final String PATH_TO_CHROME_DRIVER = "...chromedriver.exe";
    public static final String LOGIN_PAGE = "https://www.instagram.com/accounts/login/";
    public static final String CHROME_DRIVER = "http://127.0.0.1:9515";

    public static final String LOG4J_PROPERTIES_PATH = "src/main/resources/log4j.properties";
    public static final int MIN_PAUSE_MILLS = 400000;
    public static final int MAX_PAUSE_MILLS = 700000;

    public static final int TIME_OUT_IN_SECONDS = 600;

    public static final int COMMENTS_BATCH_THRESHOLD = 5;

    public static final int PAUSE_BETWEEN_COMMENTS_FROM_MS = 5000;
    public static final int PAUSE_BETWEEN_COMMENTS_TO_MS = 9000;
    public static final int PAUSE_BETWEEN_DISCOVER = 500;
}
