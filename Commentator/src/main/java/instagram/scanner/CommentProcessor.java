package instagram.scanner;

import com.github.rkumsher.collection.IterableUtils;
import com.google.common.collect.Sets;
import instagram.constant.CSSselectors;
import instagram.constant.Settings;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Set;
import java.util.concurrent.TimeUnit;

@Slf4j
public class CommentProcessor
{

    private WebDriver driver;

    public CommentProcessor(WebDriver driver)
    {
        this.driver = driver;
    }

    public String findLast() throws InterruptedException
    {
        WebElement lastCommentElement;
        lastCommentElement = locateElement(CSSselectors.LAST_COMMENT_SELECTOR);
        return lastCommentElement.getText();
    }

    public void discoverComments() throws InterruptedException
    {
        while (true)
        {
            TimeUnit.MILLISECONDS.sleep(Settings.PAUSE_BETWEEN_DISCOVER);
            driver.findElement(CSSselectors.SHOW_MORE_COMMENTS_SELECTOR).click();
        }
    }

    public WebElement locateElement(By selector) throws InterruptedException
    {
        WebElement lastCommentElement;
        while (true)
        {
            try
            {
                lastCommentElement = driver.findElement(selector);
                break;
            } catch (NoSuchElementException e)
            {
                log.error("Element is not visible yet");
            }
            TimeUnit.SECONDS.sleep(5);
            driver.navigate().refresh();
        }
        return lastCommentElement;
    }

    public String incrementCommentNumber(String number)
    {
        return String.valueOf(Integer.parseInt(number) + 1);
    }

    public String generateDog(String login)
    {
        Set<String> dogs = Sets.newHashSet(Settings.dogs);
        dogs.remove("@" + login);
        return IterableUtils.randomFrom(dogs);
    }
}
