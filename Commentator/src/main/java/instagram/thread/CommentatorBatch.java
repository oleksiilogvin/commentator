package instagram.thread;

import instagram.constant.CSSselectors;
import instagram.scanner.CommentProcessor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static instagram.constant.Settings.COMMENTS_BATCH_THRESHOLD;
import static instagram.constant.Settings.PAUSE_BETWEEN_COMMENTS_FROM_MS;
import static instagram.constant.Settings.PAUSE_BETWEEN_COMMENTS_TO_MS;

@Slf4j
public class CommentatorBatch extends AbstractCommentator
{

    public CommentatorBatch(int min, int max, String login, String password)
    {
        super(min, max, login, password);
    }

    @Override
    public void processComments(WebDriver driver, CommentProcessor commentProcessor, Actions actions) throws InterruptedException
    {
        boolean isBlocked = false;
        int threshold = 0;
        while (!isBlocked && threshold != COMMENTS_BATCH_THRESHOLD)
        {
            driver.navigate().refresh();
            String lastComment = commentProcessor.findLast();
            String preparedComment = polishComment(lastComment);
            if (NumberUtils.isCreatable(preparedComment))
            {
                preparedComment = commentProcessor.incrementCommentNumber(preparedComment);
                postComment(preparedComment, driver, actions);
                isBlocked = checkForBlocking(driver);
                logResult(driver, isBlocked, preparedComment, true);
                makeRandomPause(actions, PAUSE_BETWEEN_COMMENTS_FROM_MS, PAUSE_BETWEEN_COMMENTS_TO_MS);
                threshold++;
            }
            else
            {
                log.error("{} - is not a number! Skipped...", lastComment);
            }
        }
    }

    private boolean checkForBlocking(WebDriver driver)
    {
        try
        {
            new WebDriverWait(driver, 3).until(ExpectedConditions.visibilityOfElementLocated(CSSselectors.BLOCKED_POPUP));
            driver.findElement(CSSselectors.BLOCKED_POPUP);
        } catch (Exception e)
        {
            return false;
        }
        return true;
    }

    private String polishComment(final String targetComment)
    {
        return targetComment.replaceAll(" ", "");
    }
}
