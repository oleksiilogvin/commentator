package instagram.thread;

import instagram.constant.CSSselectors;
import instagram.constant.Settings;
import instagram.exception.InvalidLastCommentException;
import instagram.scanner.CommentProcessor;
import instagram.validator.Validator;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.DecimalFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

import static instagram.constant.Settings.COMMENTS_BATCH_THRESHOLD;
import static instagram.constant.Settings.PAUSE_BETWEEN_COMMENTS_FROM_MS;
import static instagram.constant.Settings.PAUSE_BETWEEN_COMMENTS_TO_MS;

@Slf4j
public class CommentatorDogger extends AbstractCommentator
{
    public CommentatorDogger(int min, int max, String login, String password)
    {
        super(min, max, login, password);
    }

    @Override
    public void processComments(WebDriver driver, CommentProcessor commentProcessor, Actions actions) throws InterruptedException
    {
        boolean isBlocked = false;
        int threshold = 0;
        Validator validator = new Validator();
        int randomThreshold = ThreadLocalRandom.current().nextInt(2, 4);
        while (!isBlocked && threshold != randomThreshold)
        {
            driver.navigate().refresh();
            String lastComment = commentProcessor.findLast();
            try
            {
                validator.validateDog(lastComment);
                String generated = commentProcessor.generateDog(login);
                postComment(generated, driver, actions);
                isBlocked = checkForBlocking(driver);
                log(driver, isBlocked, generated, true);
            } catch (InvalidLastCommentException e)
            {
                log.error("Invalid last comment: {}", lastComment);
            }
            makeRandomPause(actions, PAUSE_BETWEEN_COMMENTS_FROM_MS, PAUSE_BETWEEN_COMMENTS_TO_MS);
            threshold++;
        }
    }

    private boolean checkForBlocking(WebDriver driver)
    {
        try
        {
            new WebDriverWait(driver, 3).until(ExpectedConditions.visibilityOfElementLocated(CSSselectors.BLOCKED_POPUP));
            driver.findElement(CSSselectors.BLOCKED_POPUP);
        } catch (Exception e)
        {
            return false;
        }
        return true;
    }

    private void log(WebDriver driver, boolean isBlocked, String comment, boolean isSent)
    {
        String promo = Settings.promos.get(driver.getCurrentUrl());
        if (isSent && !isBlocked)
        {
            double hoursPassedFromStart = Duration.between(startTime, Instant.now()).toMinutes() / 60D;
            double totalComments = counter.incrementAndGet();
            DecimalFormat format = new DecimalFormat("#.##");
            log.info("Account: {}, Promo: {}, My: {}, Total comments: {}, Time passed: {} hours, Comments/h: {}",
                    login, promo, comment, (long) totalComments, format.format(hoursPassedFromStart),
                    format.format(totalComments / hoursPassedFromStart));
        }
    }

    private String polishComment(final String targetComment)
    {
        return targetComment.replaceAll(" ", "");
    }
}
