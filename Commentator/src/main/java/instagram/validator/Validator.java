package instagram.validator;

import instagram.exception.InvalidLastCommentException;

public class Validator
{
    public void validateDog(String comment) throws InvalidLastCommentException
    {
        if (!comment.startsWith("@"))
        {
            throw new InvalidLastCommentException();
        }
    }
}
